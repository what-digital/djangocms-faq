
Installation
===============================================================================

Run `pip install djangocms-faq`

Run `./manage.py migrate`

Update `INSTALLED_APPS` with :

```
INSTALLED_APPS = [
    ...
    'djangocms_faq',
    ...
]
```


Development
===============================================================================


/plugin contains .ts and .scss source files

to compile them into static files: 

Run `yarn install`

Run `yarn build`

