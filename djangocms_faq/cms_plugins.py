from cms import api
from cms.models import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib.admin.helpers import AdminForm
from django.http import HttpRequest
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.cms_plugins import TextPlugin

from .models import CategoryModel
from .models import QuestionModel

MODULE_NAME = 'FAQ'


@plugin_pool.register_plugin
class FAQListPlugin(CMSPluginBase):
    module = MODULE_NAME
    name = _('FAQ')
    render_template = 'faq/plugins/faq-list.html'
    allow_children = True
    child_classes = [
        'CategoryPlugin'
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        setattr(self.model, '__str__', lambda e: '')

    def render(self, context: dict, instance: dict, placeholder: dict) -> dict:
        context: dict = super().render(context, instance, placeholder)

        if instance.child_plugin_instances:
            categories = set(
                [
                    category.name for category in instance.child_plugin_instances
                ]
            )
            context['categories'] = categories
        return context


@plugin_pool.register_plugin
class CategoryPlugin(CMSPluginBase):
    module = MODULE_NAME
    model = CategoryModel
    name = _('Category')
    render_template = 'faq/plugins/category.html'
    require_parent = True
    allow_children = True
    child_classes = [
        'QuestionPlugin'
    ]
    parent_classes = [
        'FAQListPlugin'
    ]


@plugin_pool.register_plugin
class QuestionPlugin(CMSPluginBase):
    module = MODULE_NAME
    name = _('Question')
    render_template = 'faq/plugins/question.html'
    model = QuestionModel
    require_parent = True
    allow_children = True
    parent_classes = [
        'CategoryPlugin'
    ]

    def save_model(self, request: HttpRequest, obj: QuestionModel, form: AdminForm, change: bool):
        super().save_model(request, obj, form, change)
        is_create_action = not change
        if is_create_action:
            self._create_default_answer()

    def _create_default_answer(self):
        text_instance_data = dict(
            body='This is an empty question, double click on it to change it.'
        )
        text_instance: CMSPlugin = api.add_plugin(
            placeholder=self.saved_object.placeholder,
            plugin_type='AnswerPlugin',
            language=self.saved_object.language,
            target=self.saved_object,
            **text_instance_data,
        )
        text_instance.save()


@plugin_pool.register_plugin
class AnswerPlugin(TextPlugin):
    module = MODULE_NAME
    name = _('Answer')
    parent_classes = [
        'QuestionPlugin'
    ]
