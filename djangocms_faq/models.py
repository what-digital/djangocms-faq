from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import pgettext as _


class CategoryModel(CMSPlugin):
    name = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name=_('faq', 'name'),
        help_text=_('faq', "Leave it blank if the item shouldn't be related to any category")
    )

    def __str__(self):
        return self.name


class QuestionModel(CMSPlugin):
    title = models.CharField(max_length=256, verbose_name=_('faq', 'title'))

    def __str__(self):
        return self.title
