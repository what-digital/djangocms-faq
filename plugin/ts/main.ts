const Mark = require('mark.js');


export function initFaqSearch() {
    $('#faq-search').on('keyup search', (event) => {
        const searchQuery = (event.target as any).value;

        const isSearchReset = searchQuery === '';
        if (isSearchReset) {
            showAll();
        } else {
            filterQuestions(searchQuery);
        }
    });

    $('#search-form').submit((event) => {
        event.preventDefault();
    });
}


function filterQuestions(searchQuery: string) {
    $('.faq-question').each((i, question) => {
        new Mark(question).unmark();

        const questionTitle = $(question).find('.faq-question-title div').text().trim().toLowerCase();
        const questionContent = $(question).find('.faq-question-content').text().trim().toLowerCase();
        const isQuestionTitleMatched = questionTitle.includes(searchQuery);
        const isQuestionContentMatched = questionContent.includes(searchQuery);
        if (isQuestionTitleMatched || isQuestionContentMatched) {
            showQuestion(question);
            highlightQuestionTitleMatch(question, searchQuery);
        } else {
            hideQuestion(question);
        }
    });
}


function highlightQuestionTitleMatch(question: HTMLElement, searchQuery: string) {
    const instance = new Mark(question);
    instance.unmark();
    instance.mark(searchQuery, {
        "element": 'span',
        "className": 'highlighted',
        caseSensitive: false,
    });
}


function showAll() {
    $('.faq-question').each((i, question) => {
        new Mark(question).unmark();
        question.dataset.isHidden = String(false);
        collapseQuestion(question);
    });
}


function hideQuestion(question) {
    question.dataset.isHidden = String(true);
    collapseQuestion(question);
}


function showQuestion(question) {
    question.dataset.isHidden = String(false);
    expandQuestion(question);
}


function collapseQuestion(question) {
    const questionTitle = $(question).find('.faq-question-title');
    const isExpanded: boolean = questionTitle.attr('aria-expanded') === 'true';
    if (isExpanded) {
        $(question).find('.faq-question-content').collapse('toggle');
    }
}


function expandQuestion(question) {
    const questionTitle = $(question).find('.faq-question-title');
    const isCollapsed: boolean = questionTitle.attr('aria-expanded') !== 'true';
    if (isCollapsed) {
        $(question).find('.faq-question-content').collapse('toggle');
    }
}


initFaqSearch();
