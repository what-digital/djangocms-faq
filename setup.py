#!/usr/bin/env python3
from setuptools import setup


from djangocms_faq import __version__


setup(
    name='djangocms-faq',
    version=__version__,
    author='Artyom',
    author_email='artyom@what.digital',
    url='https://gitlab.com/what-digital/djangocms-faq',
    packages=[
        'djangocms_faq',
    ],
    include_package_data=True,
    install_requires=[
        'django >= 2.2',
        'django-cms <= 4.0',
    ],
)
